from PIL import Image
import sys

# Ensure the user provides an image file as argument
if len(sys.argv) != 2:
    print("Usage: python convert_to_raw.py <path_to_image>")
    sys.exit(1)

# Open the image
image_path = sys.argv[1]
img = Image.open(image_path)

# If the image has an alpha (transparency) channel, remove it
# if img.mode == 'RGBA':
#     img = img.convert('RGB')

# Save the raw pixel values to a binary file
with open("output.raw", "wb") as f:
    f.write(img.tobytes())
