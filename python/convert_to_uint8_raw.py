from PIL import Image
import numpy as np
import sys

if len(sys.argv) != 2:
    print("Usage: python convert_to_uint8_raw.py <path_to_image>")
    sys.exit(1)

# Open the image
image_path = sys.argv[1]
img = Image.open(image_path)

# Convert the image to grayscale or RGB (as needed)
# img = img.convert('L')  # Uncomment for grayscale
# img = img.convert('RGB')  # Uncomment for RGB

# Convert the image data to a numpy array and normalize to [-128, 127]
data = np.asarray(img)
# data = data.astype(np.float32)

# If your model was trained with images in [0, 255], 
# then you'd subtract 128 and cast to int8 to get values in [-128, 127]
# data = (data - 128).astype(np.int8)

# crop the image if needed
# data = data[:384,:384,:]

# Save the int8 values to a binary file
data.tofile('output_uint8.raw')
