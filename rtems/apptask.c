
#include <stdio.h>
#include <stdlib.h>
 
#include <rtems.h>
#include <rtems/bspIo.h>
#include <rtems/libcsupport.h>
#include <stdint.h>
#include "main.h"

rtems_task Application_task(
  rtems_task_argument argument
)
{
  rtems_id          tid;
  rtems_status_code status;
  unsigned int      a = (unsigned int) argument;

  status = rtems_task_ident( RTEMS_WHO_AM_I, RTEMS_SEARCH_ALL_NODES, &tid );
  char* argv[] = {"Cloud detection"};
  int argc = 1;
  printk("[RTEMS6 Application task] %s\n", argv[0]);
  int i = 0;

  setup();

  while(i<1){
    loop();
    i++;
  }

  printk("[RTEMS6 Application task] Done!\n");
  exit( 0 );
}

