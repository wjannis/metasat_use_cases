/* RTEMS headers */
#include <stdio.h>
#include <rtems.h>

//#include "omp.h"

#include <stdlib.h>

//void __attribute__((__constructor__(1000))) config_libgomp(void)
//{
//    setenv("OMP_DISPLAY_ENV", "VERBOSE", 1);
//    setenv("GOMP_SPINCOUNT", "30000", 1);
//    setenv("GOMP_DEBUG", "1", 1);
//    setenv("OMP_NUM_THREADS", "4", 1);
//}

/* forward declarations to avoid warnings */
//rtems_task Init(rtems_task_argument argument);

rtems_task Application_task(rtems_task_argument argument);

const char rtems_test_name[] = "SAMPLE SINGLE PROCESSOR APPLICATION";

#define ARGUMENT 0

static rtems_task Init( rtems_task_argument argument )
{
//    xcfPartition_t xcfPartition;

    /* Get Partition's configuration */
//    XGetMyPartitionCfg(&xcfPartition);
  
  rtems_name        task_name;  
  rtems_id          tid;
  rtems_status_code status;

  printf( "[RTEMS6 Main task] Creating and starting an application task\n");

  task_name = rtems_build_name( 'T', 'A', '1', ' ' );

  status = rtems_task_create( task_name, 1, RTEMS_MINIMUM_STACK_SIZE,
             RTEMS_INTERRUPT_LEVEL(0), RTEMS_DEFAULT_ATTRIBUTES, &tid );

  status = rtems_task_start( tid, Application_task, ARGUMENT );

  rtems_task_exit();
}

/*
#define CONFIGURE_INIT

#define CONFIGURE_MICROSECONDS_PER_TICK 10000

#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_LIBBLOCK

#define CONFIGURE_MAXIMUM_TASKS 10

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE

#define CONFIGURE_INIT_TASK_ATTRIBUTES RTEMS_FLOATING_POINT

#define CONFIGURE_UNLIMITED_OBJECTS
#define CONFIGURE_UNIFIED_WORK_AREAS

//#define CONFIGURE_MAXIMUM_PROCESSORS 4
*/

#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER

#define CONFIGURE_UNLIMITED_OBJECTS
#define CONFIGURE_UNIFIED_WORK_AREAS

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE

#define CONFIGURE_INIT

#define CONFIGURE_MAXIMUM_PROCESSORS 4

#include <rtems/confdefs.h>

