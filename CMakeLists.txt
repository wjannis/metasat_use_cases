cmake_minimum_required(VERSION 3.26)
project(metasat_use_cases)

set(CMAKE_CXX_STANDARD 20)

add_executable(metasat_use_cases src/main.cpp
        src/IDeepLearningTask.h
        src/cloud_detection/CloudDetectionTask.h
        src/ImageData.h
        src/cloud_detection/MemoryCloudImageProviderService.cpp
        src/cloud_detection/MemoryCloudImageProviderService.h)
