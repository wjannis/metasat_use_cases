#include "ImageData.h"

class IAIInferenceService {
public:
    virtual ~IAIInferenceService() {}

    virtual void initialize() = 0;
    virtual void infer(const CloudFloatImageData* pData) = 0;
//    virtual CloudIntImageData& getResults() = 0; // Assuming you have a Results class or struct
};
