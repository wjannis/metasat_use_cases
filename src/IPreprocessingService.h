class IPreprocessingService {
public:
    virtual ~IPreprocessingService() {}

    virtual void initialize() = 0;
    virtual void process(const ImageData& imgData) = 0;
    virtual ProcessedData& getProcessedData() = 0; // Assuming you have a ProcessedData class or struct
};
