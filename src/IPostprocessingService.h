class IPostprocessingService {
public:
    virtual ~IPostprocessingService() {}

    virtual void initialize() = 0;
    virtual void process(const Results& results) = 0;
    virtual FinalOutput& getFinalOutput() = 0; // Assuming you have a FinalOutput class or struct
};
