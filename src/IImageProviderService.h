#include "ImageData.h"

class IImageProviderService {
public:
    virtual ~IImageProviderService() {} // Virtual destructor to ensure derived classes get destroyed properly

    virtual void initialize() = 0;
    virtual void loadImage() = 0;
    virtual CloudFloatImageData* getImage() = 0; // Assuming you have an ImageData class or struct
};
