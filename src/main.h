#ifdef __cplusplus
#define EXTERNC extern "C"
#else
#define EXTERNC
#endif

EXTERNC void setup();
EXTERNC void loop();

#undef EXTERNC


