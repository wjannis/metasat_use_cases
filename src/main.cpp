#include <iostream>

#include "main.h"

// Include headers for each service
#include "cloud_detection/MemoryCloudImageProviderService.h"
//#include "PreprocessingService.h"
#include "cloud_detection/CloudAIInferenceService.h"
//#include "PostprocessingService.h"
//#include "OutputHandlerService.h"

// Declare service objects globally or within a namespace
MemoryCloudImageProviderService imageProvider;
//PreprocessingService preprocessor;
CloudAIInferenceService aiInference;
//PostprocessingService postprocessor;
//OutputHandlerService outputHandler;

// Keeping track of loop iteration
int i = 0;

void setup() {
//  std::cout << "[Setup] Initializing.." << std::endl;

  // Initialization code for each service
//  imageProvider.initialize();
//  preprocessor.initialize();
  aiInference.initialize();
//  postprocessor.initialize();
//  outputHandler.initialize();
}

void loop() {
  //std::cout << "[Loop] Number: " << i << std::endl;

  // Main processing loop for each service

//  // 1. Load the image
  imageProvider.loadImage();
//
//  // 2. Preprocess the image
//  preprocessor.process(imageProvider.getImage()); // Assuming getImage returns a reference or pointer to the loaded image
//
//  // 3. AI inference
  aiInference.infer(imageProvider.getImage()); // Assuming getProcessedData returns processed image/data
//
//  // 4. Post-process the results
//  postprocessor.process(aiInference.getResults()); // Assuming getResults returns inference results
//
//  // 5. Handle the output
//  outputHandler.handleOutput(postprocessor.getFinalOutput()); // Assuming getFinalOutput returns post-processed data/results

  i++;
}

int main() {
  std::cout << "[Main] Start up.." << std::endl;

  setup();

  // Deployment: Infinite loop to keep calling loop()
  while (i < 1) {
    loop();
  }

  return 0;
}
