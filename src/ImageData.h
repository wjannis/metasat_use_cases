//
// Created by wjannis on 25/10/23.
//

#ifndef METASAT_USE_CASES_IMAGEDATA_H
#define METASAT_USE_CASES_IMAGEDATA_H

#include <cstddef>
#include <cstdint>

const int CLOUDIMAGELENGTH = 589824;  // 384*348*4
const int SHIPIMAGELENGTH = 1769472;  // 768*768*3

struct CloudFloatImageData {
  float data[CLOUDIMAGELENGTH];
  size_t length;
};

struct ShipIntImageData {
  uint8_t data[SHIPIMAGELENGTH];
  size_t length;
};

#endif // METASAT_USE_CASES_IMAGEDATA_H
