//
// Created by wjannis on 25/10/23.
//

//#include <fstream>

#include "CloudAIInferenceService.h"
#include "CloudModelInt8.h"

#include <iostream>
#include <ctime>

#ifdef USE_PRINTK
    #include "rtems/bspIo.h"
    #define PRINT(...) printk(__VA_ARGS__)
#else
    #include <stdio.h>
    #define PRINT(...) printf(__VA_ARGS__)
#endif

#include <string>

#define TF_LITE_ENSURE_STATUS_(a)  \
  do {                             \
    const TfLiteStatus s = (a);    \
    if (s != kTfLiteOk) {          \
      PRINT("Fail: %d\n", s);     \
    }                              \
  } while (0)

namespace {
using OpResolver = tflite::MicroMutableOpResolver<10>;

  TfLiteStatus RegisterOps(OpResolver& op_resolver) {
    TF_LITE_ENSURE_STATUS(op_resolver.AddLogistic());
    TF_LITE_ENSURE_STATUS(op_resolver.AddQuantize());
    TF_LITE_ENSURE_STATUS(op_resolver.AddConcatenation());
    TF_LITE_ENSURE_STATUS(op_resolver.AddConv2D());
    TF_LITE_ENSURE_STATUS(op_resolver.AddMaxPool2D());
    TF_LITE_ENSURE_STATUS(op_resolver.AddResizeBilinear());
    TF_LITE_ENSURE_STATUS(op_resolver.AddDequantize());
    TF_LITE_ENSURE_STATUS(op_resolver.AddMul());
    TF_LITE_ENSURE_STATUS(op_resolver.AddAdd());
    TF_LITE_ENSURE_STATUS(op_resolver.AddFullyConnected());
    return kTfLiteOk;
  }
}

namespace {
  int inference_count = 0;
  OpResolver op_resolver;
}  // namespace



CloudAIInferenceService::~CloudAIInferenceService() {}
void CloudAIInferenceService::initialize() {
  // tflite::InitializeTarget();

  // Map the model into a usable data structure. This doesn't involve any
  // copying or parsing, it's a very lightweight operation.
  model = tflite::GetModel(model_quant_tflite);

  // This pulls in all the operation implementations we need.
  RegisterOps(op_resolver);

  // Create an interpreter object and a pointer for accessibility
  interpreter = new tflite::MicroInterpreter(model, op_resolver, tensor_arena, kTensorArenaSize);

  // Allocate memory from the tensor_arena for the model's tensors.
  TfLiteStatus allocate_status = interpreter->AllocateTensors();
  if (allocate_status != kTfLiteOk) {
    // std::cout << "AllocateTensors() failed" << std::endl;
    PRINT("AllocateTensors() failed\n");
    return;
  }

  // Obtain pointers to the model's input and output tensors.
  input = interpreter->input(0);
  output = interpreter->output(0);
}

void CloudAIInferenceService::infer(const CloudFloatImageData *pData) {
  // Copy image into models input buffer
  for(int j = 0; j < 384*384*4; ++j)
    input->data.f[j] = (float) *(pData->data + j);

  clock_t start = clock();

  // Inference is done here
  TF_LITE_ENSURE_STATUS_(interpreter->Invoke());
  
  clock_t end = clock();
  double optimizedTime = double(end - start) / CLOCKS_PER_SEC;

  std::cout << "Execution time: " << optimizedTime << " seconds" << std::endl;
  //PRINT("!execution time: %f\n", (float) optimizedTime);
  // Calculate cloud coverage
  int p = 0;
  //std::ofstream myFile ("/home/wjannis/image.txt");
  for (int i = 0; i < 384*384; ++i) {
    //myFile << output->data.f[i] << " ";
    if (output->data.f[i] > 0.5)
      ++p;
  }

  std::cout << "Expected Cloud Coverage = 0.406433" << std::endl;
  std::cout << "Predicted Cloud Coverage = " << (float) p / (384*384) << std::endl;
//  PRINT("Predicted Cloud Coverage: %f\n", (float) p/(384*384)); 
}

//CloudIntImageData &CloudAIInferenceService::getResults() { return (CloudIntImageData &)nullptr; }
