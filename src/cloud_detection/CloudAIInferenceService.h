//
// Created by wjannis on 25/10/23.
//

#ifndef METASAT_USE_CASES_CLOUDAIINFERENCESERVICE_H
#define METASAT_USE_CASES_CLOUDAIINFERENCESERVICE_H

#include "../IAIInferenceService.h"

#include "tensorflow/lite/core/c/common.h"
#include "tensorflow/lite/micro/micro_mutable_op_resolver.h"
#include "tensorflow/lite/micro/micro_interpreter.h"
#include "tensorflow/lite/schema/schema_generated.h"

class CloudAIInferenceService : IAIInferenceService{
  public:
    ~CloudAIInferenceService() override;
    void initialize() override;
    void infer(const CloudFloatImageData *pData) override;
  
  private:
    const tflite::Model* model = nullptr;
    tflite::MicroInterpreter* interpreter = nullptr;
    TfLiteTensor* input = nullptr;
    TfLiteTensor* output = nullptr;
    static const int kTensorArenaSize = 3000000;  // Actual: 2949120
    uint8_t tensor_arena[kTensorArenaSize];

};

#endif // METASAT_USE_CASES_CLOUDAIINFERENCESERVICE_H
