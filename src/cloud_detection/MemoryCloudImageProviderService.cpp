//
// Created by wjannis on 25/10/23.
//

#include "MemoryCloudImageProviderService.h"
#include "CloudImageFP32.h"
#include <iostream>

MemoryCloudImageProviderService::MemoryCloudImageProviderService() {}
MemoryCloudImageProviderService::~MemoryCloudImageProviderService() {}
void MemoryCloudImageProviderService::initialize() {
  //std::cout << "[MemoryCloudImageProviderService] Initialized!" << std::endl;
}
void MemoryCloudImageProviderService::loadImage() {
  //std::cout << "[MemoryCloudImageProviderService] Load image.." << std::endl;

  image = reinterpret_cast<CloudFloatImageData *>(&output_fp32_384_raw);
//  image->length = output_fp32_384_raw_len;

  uint8_t pixel = image->data[0];
  //std::cout << "First pixel value is " << (int) pixel << std::endl;
}
CloudFloatImageData* MemoryCloudImageProviderService::getImage() {
  return image;
}
