//
// Created by wjannis on 25/10/23.
//

#ifndef METASAT_USE_CASES_MEMORYCLOUDIMAGEPROVIDERSERVICE_H
#define METASAT_USE_CASES_MEMORYCLOUDIMAGEPROVIDERSERVICE_H

#include "../IImageProviderService.h"
#include <cstdint>

class MemoryCloudImageProviderService : IImageProviderService{
public:
  MemoryCloudImageProviderService();
  ~MemoryCloudImageProviderService() override;
  void initialize() override;
  void loadImage() override;
  CloudFloatImageData* getImage() override;

private:
  CloudFloatImageData* image;
};

#endif // METASAT_USE_CASES_MEMORYCLOUDIMAGEPROVIDERSERVICE_H
