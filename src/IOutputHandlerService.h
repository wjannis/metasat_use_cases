class IOutputHandlerService {
public:
    virtual ~IOutputHandlerService() {}

    virtual void initialize() = 0;
    virtual void handleOutput(const FinalOutput& fOutput) = 0;
};
