//
// Created by wjannis on 11/10/23.
//

#ifndef METASAT_USE_CASES_IDEEPLEARNINGTASK_H
#define METASAT_USE_CASES_IDEEPLEARNINGTASK_H

#include <cstdint>
#include <string>

class IDeepLearningTask {
  public:
    virtual ~IDeepLearningTask() = default; // Virtual destructor
    virtual void loadModel(const std::string& path) = 0;
    virtual void loadImageData(uint8_t* data, size_t& size) = 0;
    virtual void preprocessData(uint8_t* data) = 0;
    virtual void runInference(tflite::interpreter* interpreter, uint8_t* data) = 0;
    virtual void postprocessData(uint8_t* data) = 0;
    virtual void calculateMetric(uint8_t* pred) = 0;

};

#endif // METASAT_USE_CASES_IDEEPLEARNINGTASK_H
